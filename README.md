# VIMRC

My vim config files

## Install

1. git clone http://bitbucket.org/galmeida/vimrc.git ~/.vim
1. ln -s ~/.vim/vimrc ~/.vimrc
1. cd ~/.vim
1. sh ./initsubmodules.sh

### SimpleNote set-up

You must have a [SimpleNote](http://www.simplenoteapp.com/) account.

1. cp ~/.vim/simplenoterc.template ~/.vim/simplenoterc
1. vim ~/.vim/simplenoterc  *# and setup simplenote's user and password*

### Plugins included
1. see .gitmodules [here](src/)

### Adding a new vim plugin from github

1.  git submodule add http://bitbucket.org/user/plugin.git bundle/plugin   (or...)
1.  git submodule add git://github.com/user/plugin.git bundle/plugin
