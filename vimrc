version 5.6
" pathogen
  source ~/.vim/bundle/vim-pathogen/autoload/pathogen.vim
  call pathogen#infect()

" simplenote
  silent! source ~/.vim/simplenoterc

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  syntax on
  set icon
  let &titlestring = expand ("%:p:~:.:h")
" make searches case-insensitive, unless they contain upper-case letters:
  set ignorecase
  set smartcase
" show the `best match so far' as search strings are typed:
  set incsearch
" Turn on the WiLd menu
  set wildmenu
  set wildmode=list:longest,full
" Set 7 lines to the cursor - when moving vertically using j/k
  set so=7
" Don't redraw while executing macros (good performance config)
  set lazyredraw
" Show matching brackets when text indicator is over them
  set showmatch
" don't make it look like there are line breaks where there aren't:
  set nowrap
  set showbreak=>\ 
" use "[RO]" for "[readonly]" to save space in the message line:
  set shortmess+=r
" always report lines changed
  set report=0  
" have fifty lines of command-line (etc) history:
  set history=50
" show command in the last line of the screen
  set showcmd
" no fucking search highlight
  set nohlsearch

" => colors and fonts
  set background=dark
  colorscheme torte
  set gfn=Monaco:h12
" Set extra options when running in GUI mode
  if has("gui_running")
    set fuoptions=maxvert,background:#00000000
    set guioptions-=T
    set t_Co=256
  endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => file management
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  set enc=utf-8
  set fileformats=unix,dos,mac
  set nobackup
  set nowritebackup
  set suffixes=.bak,~,.info,.o,.aux,.bak,.dvi,.gz,.idx,.log,.ps,.swp,.tar,CVS/,.svn/
  set viminfo=/10,'10,r/mnt/zip,r/mnt/floppy,f0,h,\"100,r/Volumes

" Return to last edit position when opening files (You want this!)
  autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
  

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => text editing
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" use indents of 4 spaces, and have them copied down lines:
  set tabstop=4
  set softtabstop=4
  set shiftwidth=4
  set expandtab
  set smarttab
  set shiftround
  set autoindent
" autoformat text
  set formatoptions="+tc"
" no max line len
  set textwidth=0
" enable cursor keys in edit mode
  set esckeys
" enable modeline
  set modelines=1
  set modeline
" CRTL-P/CTRL-N behavior
  set complete=.,i,],b,w

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => hot keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  set pastetoggle=<F4>
  map <F5>  :bp<C-M>
  map <F6>  :bn<C-M>

  let shell="bash"
  map <C-Z> :shell
" Treat long lines as break lines (useful when moving around in them)
  map j gj
  map k gk
  noremap <Down> gj
  noremap <Up> gk
"  set highlight=8r,db,es,hs,mb,Mr,nu,rs,sr,tb,vr,ws
"  set path=.,~/.vim,$VIM/syntax/

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => file types
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype on
filetype plugin indent on

augroup filetype
  autocmd BufNewFile,BufRead .followup,.letter,mutt-*,nn.*,snd.*  set filetype=mail
  autocmd BufNewFile,BufRead *.txt,TODO,INSTALL,README,BUGS       set filetype=human
  autocmd BufNewFile,BufRead *.textile                            set filetype=textile
  autocmd BufNewFile,BufRead *.xml,*.XML                          set filetype=xml
  autocmd BufNewFile,BufRead *.groovy                             set filetype=java
augroup END

autocmd FileType php,perl,c   set nowrap expandtab textwidth=0  formatoptions+=ro si cindent
autocmd FileType cpp,java     set nowrap expandtab textwidth=0  formatoptions+=ro si cindent
autocmd FileType sh,phyton    set nowrap expandtab textwidth=0  formatoptions+=ro
autocmd FileType mail,human   set wrap   expandtab textwidth=72 formatoptions+=t
autocmd FileType css          set        expandtab                                si
autocmd FileType html,css,xml set        expandtab textwidth=134 ts=2 sts=2 sw=2
autocmd FileType html         set                               formatoptions+=tl
autocmd FileType make         set        noexpandtab
autocmd FileType mkd          set wrap   expandtab textwidth=0  formatoptions+=tl linebreak
autocmd FileType mkd          nnoremap <leader>m :silent !open -a Marked.app '%:p'<cr>
autocmd FileType textile      set wrap   expandtab textwidth=0  formatoptions+=tl linebreak

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => abreviations
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  iab YTS   <C-R>=strftime("%Y-%m-%d %T")<CR>
" Example: 1997-10-27 12:00:00
  iab YDATE <C-R>=strftime("%a %b %d %T %Z %Y")<CR>
" Example: Tue Dec 16 12:07:00 CET 1997


" vim: ts=2 sts=2 et sw=2 tw=70
