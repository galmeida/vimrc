#!/bin/sh
FIND=`find . -name .gitmodules`
BASEDIR=$PWD
LAST=0
CURRENT=`find . -name .gitmodules | wc -l`
while [ $LAST -ne $CURRENT ]; do
    LAST=$CURRENT
    for FILE in `find . -name .gitmodules`; do
        DIR=`dirname $FILE`
        (cd $DIR; git submodule init; git submodule update)
        RUN=1
    done
    CURRENT=`find . -name .gitmodules | wc -l`
done
